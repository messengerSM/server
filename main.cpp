#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <map>
#include <utility>
#include <vector>
#include <string>
#include <iostream>
#include <errno.h>
#include <algorithm>

struct Package{
    char nick[20];
    char buff[1024];
};

struct THREADINFO {
    pthread_t thread_ID;
    int sockfd;
    std::string nick;
    pthread_mutex_t userMutexSend;
};

// sending to all connected clients the list of online users
void sendClientList();

//with this function server receiving message from client and send to the client whos name is shown as receiver
//this function is given to the client thread as an argument
void * talkWithClient(void * ptr);

// creating server socket in this function
void createServer();

// listen and scceptin new connections for clients and creating new threads  for data transfer with them
void acceptingClients();

//sending message to user that signed in package
void sendMessageToUser(Package & pack,THREADINFO & myThreadInfo);

//sending message to all users
void sendMessageToAll(Package & pack,THREADINFO & myThreadInfo);

//erase client from client_list when client disconnecting
void eraseClient(THREADINFO & myThreadInfo);

std::vector<THREADINFO> client_list;

struct sockaddr_in serv_addr, cli_addr;

//Socket File Descriptor
int sockfd;

int err_ret;

// port number for connection
int portno;

int main(int argc, char ** argv){

    createServer();

    acceptingClients();

    return 0;
}


void * talkWithClient(void *ptr){

    THREADINFO myThread = *((THREADINFO *)ptr);;

    while(true){
        int check;
        struct Package pack;
        bzero(&pack, sizeof(Package));
        for (int i = 0; i < sizeof(Package); i++){
            check = recv(myThread.sockfd,(void*)&pack+i,1,0);
        }
        if (check == 0)
           break;

        std::string nameToSend(pack.nick);
        if (nameToSend != "Send to all"){
            sendMessageToUser(pack,myThread);
        }else{
            sendMessageToAll(pack,myThread);
        }
    }

    eraseClient(myThread);
    sendClientList();
    pthread_exit(0);
}

void createServer(){

    printf("enter port\n");
    std::cin>>portno;

    printf("create Socket\n");
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0){
        err_ret = errno;
        std::cerr << "socket() failed..." <<std::endl;
                exit(err_ret);
    }

    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_port = htons(portno);

    printf("BIND socket\n");
    if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0){
        err_ret = errno;
        std::cerr << "bind() failed..." <<std::endl;
        exit(err_ret);
    }
}

void acceptingClients(){

    printf("listening clients\n");
    listen(sockfd,50);
    socklen_t clilen = sizeof(cli_addr);

    while (true) {
        printf("wait for accept client\n");
        int newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);
        if (newsockfd >= 0){
            THREADINFO threadinfo;
            threadinfo.sockfd = newsockfd;
            int bytes;
            while(true){
                char name[20];
                bzero(name,20);
                char answer = '1';
                printf("recieve user name\n");
                for(int i = 0; i < 20;i++){
                    recv(threadinfo.sockfd,(void*)(name+i),1,0);
                }
                printf(" ---- uname of connection - %s\n",name);
                std::vector<THREADINFO>::iterator it = client_list.begin();
                for(it;it!=client_list.end();it++){
                    if(std::string(it->nick) == std::string(name)){
                        printf("uname not valid\n");
                        answer = '0';
                        send(threadinfo.sockfd,(void *)&answer,1,0);
                    }
                }
                if(answer == '1'){
                    printf("valid uname\n");
                    threadinfo.nick = name;
                    printf("uname is %s\n",threadinfo.nick);
                    send(threadinfo.sockfd,(void *)&answer,1,0);
                    break;
                }
            }

            client_list.push_back(threadinfo);
            pthread_mutex_init(&client_list.back().userMutexSend,NULL);
            printf("create client thread\n");
            pthread_create(&threadinfo.thread_ID,NULL,talkWithClient,(void*)&client_list.back());
            sendClientList();
        }
    }
}

void sendClientList(){
    std::string clList;
    std::vector<THREADINFO>::iterator it = client_list.begin();
    for(it; it != client_list.end(); it++){
        clList += it->nick;
        clList += " ";
    }

    std::string onlinelist = "onlinelist";
    Package pack;
    bzero(&pack,sizeof(Package));
    for (int i = 0;i<10;i++)
        pack.nick[i] = onlinelist[i];
    for (int i = 0;i<clList.length();i++)
        pack.buff[i] = clList[i];
    std::cout <<"pack.nick" <<pack.nick << std::endl;
    std::cout << "pack.buff"<<pack.buff << std::endl;
    it = client_list.begin();
    for(it; it != client_list.end(); it++){
        pthread_mutex_lock(&it->userMutexSend);
        std::cout << "sending List of Online Users to -- " << it->nick << std::endl;
        for(int i = 0; i < sizeof(Package); i++){
            send(it->sockfd,(void *)((char *)&pack + i),1,0);
        }
        pthread_mutex_unlock(&it->userMutexSend);
    }
}

void sendMessageToUser(Package &pack, THREADINFO & myThreadInfo){

    std::string nameToSend(pack.nick);

    THREADINFO * new_t;

    std::vector<THREADINFO>::iterator it;
    for (it = client_list.begin(); it != client_list.end(); it++){
        if (it->nick == nameToSend){
            new_t = &(*it);
            break;
        }
    }
    bzero(pack.nick,20);
    for(int i = 0; i < myThreadInfo.nick.length(); i++)
        pack.nick[i] = myThreadInfo.nick[i];

    pthread_mutex_lock(&new_t->userMutexSend);
    for (int i = 0; i < sizeof(Package); i++){
        send(new_t->sockfd,(void *)((char *)&pack + i),1,0);
    }
    pthread_mutex_unlock(&new_t->userMutexSend);
}

void sendMessageToAll(Package &pack, THREADINFO & myThreadInfo){

    bzero(pack.nick,20);

    for(int i = 0; i < myThreadInfo.nick.length(); i++)
        pack.nick[i] = myThreadInfo.nick[i];

    std::vector<THREADINFO>::iterator it;

    std::cout << "sending message to All-- " << myThreadInfo.nick << std::endl;

    for(it = client_list.begin(); it != client_list.end(); it++){
        if(it->nick != myThreadInfo.nick){
            pthread_mutex_lock(&it->userMutexSend);
            for(int i = 0; i < sizeof(Package); i++){
                send(it->sockfd,(void *)((char *)&pack + i),1,0);
            }
            pthread_mutex_unlock(&it->userMutexSend);
        }
    }
}


void eraseClient(THREADINFO & myThreadInfo){
    std::vector<THREADINFO>::iterator it = client_list.begin();
    for(it; it != client_list.end(); it++){
        if(it->nick == myThreadInfo.nick){
            client_list.erase(it);
            break;
        }
    }
}
